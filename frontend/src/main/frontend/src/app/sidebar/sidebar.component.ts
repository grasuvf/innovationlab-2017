import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-sidebar-menu',
  styleUrls: ['./sidebar.component.scss'],
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent {
  @Input() adminMode: number;
  small_menu: boolean = true;

  toggleSidebarMenu() {
    this.small_menu = !this.small_menu;
  }
}
