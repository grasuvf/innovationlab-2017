import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import * as jsPDF from "jsPDF";

import { PaginationService } from '../_services/pagination.service';

@Component({
  selector: 'app-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
  providers: [PaginationService]
})
export class DashboardComponent implements OnInit {
  constructor(private http: Http, private paginationService: PaginationService) { }

  private employeesData = [];
  employeesDataNames = [];
  employeeListRange: number[] = [5, 10, 50, 100, 250, 500, 1000, 2500, 5000];
  pager: any = {};
  pagedItems: any[];
  pageSize: number = 5;
  sPageSize: number = 5;
  resultsNumber: number = this.pageSize;

  sortDirection: boolean = false;
  sortColumn: string;

  ngOnInit() {
    this.http.get('http://localhost:8080/api/columns/specific_columns_employees')
      .map((response: Response) => response.json())
      .subscribe(data => {
        this.employeesDataNames = data;
      });

    this.http.get('http://localhost:8080/api/employees')
      .map((response: Response) => response.json())
      .subscribe(data => {
        this.employeesData = data;

        this.setPage(1);
      });
  }

  toggleSortOrder(sortColumn, property) {
    this.sortDirection = this.sortDirection ? false : true;
    this.sortColumn = sortColumn;
    property.boolVal = !property.boolVal ;
  }

  selectPageSize(ps) {
    this.pageSize = ps;
    this.sPageSize = ps;
    this.setPage(1);
  }

  updatePagination(s) {
    if(s) {
      if(this.pageSize != 5000){
        this.pageSize = 5000;
        this.setPage(1);
      }
    } else {
      this.selectPageSize(this.sPageSize);
    }
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) return;

    this.pager = this.paginationService.getPager(this.employeesData.length, page, this.pageSize);
    this.pagedItems = this.employeesData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  changeResultsNumber(rez: number) {
    this.resultsNumber = rez;
  }

  saveRaport() {
    var doc = new jsPDF();
    var raport = [];

    for(var it = 0; it < this.employeesDataNames.length; it++)
    {
      raport.push(this.employeesDataNames[it][1]);
      raport.push("\n");
    }
    doc.text(raport, 10, 10);
    doc.save('Raport.pdf');
  }
}
