import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router, ActivatedRoute, Params, Data } from '@angular/router';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-project',
  styleUrls: ['./project.component.scss'],
  templateUrl: './project.component.html'
})
export class ProjectComponent implements OnInit {
  constructor(private http: Http, private route: ActivatedRoute) { }

  id: number;
  private project = [];
  private projectsData = [];

  ngOnInit() {
    this.http.get('http://localhost:8080/api/projects/'+ this.route.snapshot.params['id'])
      .map((response: Response) => response.json())
      .subscribe(data => {
        this.project = data;
      });

    this.http.get('http://localhost:8080/api/projects')
      .map((response: Response) => response.json())
      .subscribe(data => {
        this.projectsData = data;
      });
  }
  
}
