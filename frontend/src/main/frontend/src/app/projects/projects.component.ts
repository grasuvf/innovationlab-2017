import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { PaginationService } from '../_services/pagination.service';

@Component({
  selector: 'app-projects',
  styleUrls: ['./projects.component.scss'],
  templateUrl: './projects.component.html',
  providers: [PaginationService]
})
export class ProjectsComponent implements OnInit {
  constructor(private http: Http, private paginationService: PaginationService) { }

  private projectsData = [];
  projectListRange: number[] = [5, 10, 50, 100, 250, 500, 1000];
  pager: any = {};
  pagedItems: any[];
  pageSize: number = 5;
  sPageSize: number = 5;

  ngOnInit() {
    this.http.get('http://localhost:8080/api/projects')
      .map((response: Response) => response.json())
      .subscribe(data => {
        this.projectsData = data;

        this.setPage(1);
      });
  }

  selectPageSize(ps) {
    this.pageSize = ps;
    this.sPageSize = ps;
    this.setPage(1);
  }

  updatePagination(s) {
    if(s) {
      if(this.pageSize != 1000){
        this.pageSize = 1000;
        this.setPage(1);
      }
    } else {
      this.selectPageSize(this.sPageSize);
    }
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) return;

    this.pager = this.paginationService.getPager(this.projectsData.length, page, this.pageSize);
    this.pagedItems = this.projectsData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
