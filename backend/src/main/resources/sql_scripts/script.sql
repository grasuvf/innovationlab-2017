DROP SCHEMA IF EXISTS public CASCADE;

CREATE SCHEMA public;

DROP TABLE IF EXISTS employees CASCADE;
DROP TABLE IF EXISTS projects CASCADE;
DROP TABLE IF EXISTS employee_project CASCADE;

SET datestyle = "ISO, DMY";

CREATE TABLE employees (
	employee_id SERIAL PRIMARY KEY,
	id_pas CHARACTER VARYING(50) UNIQUE NOT NULL,
	employee_name CHARACTER VARYING(50),
	employee_name_pas CHARACTER VARYING(50),
	grade_pas CHARACTER VARYING(50),
	project_code_pas CHARACTER VARYING(50),
	discipline CHARACTER VARYING(30),
	core_skill CHARACTER VARYING(50),
	booking_status CHARACTER VARYING(50),
	allocation_status CHARACTER VARYING(50),
	actual_release_date TIMESTAMP WITHOUT TIME ZONE,
	release_date TIMESTAMP WITHOUT TIME ZONE,
	booked CHARACTER VARYING(50),
	booked_at_date TIMESTAMP WITHOUT TIME ZONE,
	soft_booked_for CHARACTER VARYING(50),
	booked_for CHARACTER VARYING(50),
	color CHARACTER VARYING(50),
	username_pas CHARACTER VARYING(50),
	maternity_leaver CHARACTER VARYING(50),
	discipline_main_skill CHARACTER VARYING(50),
	employee_comments TEXT,
	step_to_grade CHARACTER VARYING(50),
	endava_profile CHARACTER VARYING(50),
	skill_extended TEXT,
	leadership_skills CHARACTER VARYING(50),
	people_manager INTEGER REFERENCES employees(employee_id)
);

INSERT INTO employees VALUES (
    DEFAULT
    ,'HR7364'
    ,'Bicsabi Claudiu'
    ,'Claudiu Bicsabi'
    ,'TL'
    ,'PRSDV001'
    ,'Testing'
    ,'Manual Testing'
    ,'Bench in 1 MO'
    ,'Bench in 1 MO'
    ,'1/6/2017'
    ,'1/6/2017'
    ,'No'
    ,null
    ,null
    ,null
    ,11
    ,'cbicsabi'
    ,null
    ,'Manual Testing'
    ,null
    ,null
    ,'Yes'
    ,null
    ,null
    ,null
);

CREATE TABLE projects (
	project_id SERIAL PRIMARY KEY,
	project_code_pas CHARACTER VARYING(50) UNIQUE,
	agu_pas CHARACTER VARYING(50),
	account_pas CHARACTER VARYING(50),
	project_name_pas CHARACTER VARYING(50),
	business_owner_pas INTEGER REFERENCES employees(employee_id),
	release_date_pas TIMESTAMP WITHOUT TIME ZONE,
	description CHARACTER VARYING(200),
	start_date TIMESTAMP WITHOUT TIME ZONE,
	actual_release_date TIMESTAMP WITHOUT TIME ZONE,
	contact_person INTEGER REFERENCES employees(employee_id),
	technologies CHARACTER VARYING(100),
	looking_for_people BOOLEAN,
	city CHARACTER VARYING(50)
);

INSERT INTO projects VALUES (
    DEFAULT
    ,'RSACQ01'
    ,'TPU'
    ,'Nets'
    ,'User Experience'
    ,1
    ,'31/7/2020'
    ,'Super'
    ,'31/7/2015'
    ,'31/7/2019'
    ,1
    ,'Java8,Spring'
    ,true
    ,'Iasi'
);


CREATE TABLE employee_project (
	id SERIAL PRIMARY KEY,
	employee_id INTEGER REFERENCES employees(employee_id),
	project_id INTEGER REFERENCES projects(project_id),
	join_date TIMESTAMP WITHOUT TIME ZONE,
	leave_date TIMESTAMP WITHOUT TIME ZONE,
	project_role CHARACTER VARYING(50)
) ;

INSERT INTO employee_project VALUES (
    DEFAULT
    ,1
    ,1
    ,'31/7/2020'
    ,'31/7/2021'
    ,'TESTER IUHUUU');

DROP VIEW IF EXISTS table_metadata;
DROP TABLE IF EXISTS table_metadata;

CREATE TABLE table_metadata (
  column_name CHARACTER VARYING(50),
  table_name  CHARACTER VARYING(50),
  column_display_name  CHARACTER VARYING(50),
  is_editable BOOLEAN,
  PRIMARY KEY(column_name, table_name)
);

INSERT INTO table_metadata (column_name, table_name)
SELECT column_name, table_name FROM information_schema.columns WHERE table_schema = 'public';
