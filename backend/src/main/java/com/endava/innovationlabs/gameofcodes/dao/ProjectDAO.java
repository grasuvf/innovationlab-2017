package com.endava.innovationlabs.gameofcodes.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.endava.innovationlabs.gameofcodes.model.Project;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ProjectDAO extends AbstractDAO {

    protected ProjectDAO() {
        super(Project.class);
    }

	public List<Project> getAllProjects() {
		return entityManager.createQuery("SELECT proj FROM Project proj")
				.getResultList();
	}
}