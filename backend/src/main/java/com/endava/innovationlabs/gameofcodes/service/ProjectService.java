package com.endava.innovationlabs.gameofcodes.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.endava.innovationlabs.gameofcodes.dao.ProjectDAO;
import com.endava.innovationlabs.gameofcodes.model.Project;
import com.endava.innovationlabs.gameofcodes.repository.ProjectRepository;
import com.endava.innovationlabs.gameofcodes.repository.TableMetadataRepository;


@Service
public class ProjectService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ProjectService.class);
    
    @Autowired
    private ProjectDAO projectDAO;
    
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TableMetadataRepository tableMetadataRepository;

    public List<Project> getAllProjects() {
        return projectDAO.getAllProjects();
    }

    public Project getProjectById(Integer id) {
        return projectRepository.findProjectByProjectId(id);
    }

    public List getColumnNames() {
        return tableMetadataRepository.findTableMetadatasByTableName("projects");
    }

    public void updateProject(Project project) {
        LOGGER.info("Updating project with id: " + project.getProjectId());
        projectRepository.save(project);
    }
}
