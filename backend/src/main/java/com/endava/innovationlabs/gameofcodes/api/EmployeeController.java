package com.endava.innovationlabs.gameofcodes.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.endava.innovationlabs.gameofcodes.model.Employee;
import com.endava.innovationlabs.gameofcodes.model.TableMetadata;
import com.endava.innovationlabs.gameofcodes.service.EmployeeService;
import com.endava.innovationlabs.gameofcodes.service.TableMetadataService;

@RestController
@RequestMapping(value = "/api/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TableMetadataService tableMetadataService;
    
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Employee>> getEmployees() {
        try {
            return new ResponseEntity<>(employeeService.getAllEmployees(), HttpStatus.OK);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ArrayList<Employee>(), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Integer id) {
        return new ResponseEntity<>(employeeService.getEmployeeById(id), HttpStatus.OK);
    }
    
    @CrossOrigin
    @RequestMapping(value = "column_names", method = RequestMethod.GET)
    public ResponseEntity<List<TableMetadata>> getColumnNames() {
        return new ResponseEntity<>(tableMetadataService.getColumnNames("employees"), HttpStatus.OK);
    }

    @RequestMapping(value = "column_name/{columnName}/{newColumnName}", method = RequestMethod.PUT)
    public ResponseEntity updateColumnDisplayName(@PathVariable String columnName, @PathVariable String newColumnName) {
        try {
            tableMetadataService.updateColumnDisplayName("employees", columnName, newColumnName);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity("{exception:" + e.getStackTrace() +"}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseEntity updateEmployee(@RequestBody Employee employee) {
        employeeService.updateEmployee(employee);
        return new ResponseEntity(HttpStatus.OK);
    }
}
