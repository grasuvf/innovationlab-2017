package com.endava.innovationlabs.gameofcodes.repository;

import com.endava.innovationlabs.gameofcodes.model.TableMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import java.util.List;

public interface TableMetadataRepository extends JpaRepository<TableMetadata, TableMetadata.TableMetadataKey> {

    List<TableMetadata> findTableMetadatasByTableName(String tableName);
}
