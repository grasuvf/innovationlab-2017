package com.endava.innovationlabs.gameofcodes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "projects")
public class Project implements Serializable {

	@OneToMany(mappedBy = "project")
	private Set<EmployeeProject> employeeProjects = new HashSet<EmployeeProject>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer projectId;

	@Column(name = "project_code_pas")
	private String projectCodePas;

	@Column(name = "agu_pas")
	private String aguPas;

	@Column(name = "account_pas")
	private String accountsPas;

	@Column(name = "project_name_pas")
	private String projectNamePas;

	@OneToOne(cascade = { CascadeType.REMOVE })
	@JoinColumn(name = "business_owner_pas")
	private Employee businessOwnerPas;

	@Temporal(TemporalType.DATE)
	@Column(name = "release_date_pas")
	private Date releaseDatePas;

	@Column(name = "description")
	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "actual_release_date")
	private Date actualReleaseDate;

	@OneToOne(cascade = { CascadeType.REMOVE })
	@JoinColumn(name = "contact_person")
	private Employee contactPerson;

	@Column(name = "technologies")
	private String technologies;

	@Column(name = "looking_for_people")
	private boolean lookingForPeople;

	@Column(name = "city")
	private String city;

	@JsonIgnore
	public Set<EmployeeProject> getEmployeeProjects() {
		return employeeProjects;
	}

	public void setEmployeeProjects(Set<EmployeeProject> employeeProjects) {
		this.employeeProjects = employeeProjects;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectCodePas() {
		return projectCodePas;
	}

	public void setProjectCodePas(String projectCodePas) {
		this.projectCodePas = projectCodePas;
	}

	public String getAguPas() {
		return aguPas;
	}

	public void setAguPas(String aguPas) {
		this.aguPas = aguPas;
	}

	public String getAccountsPas() {
		return accountsPas;
	}

	public void setAccountsPas(String accountsPas) {
		this.accountsPas = accountsPas;
	}

	public String getProjectNamePas() {
		return projectNamePas;
	}

	public void setProjectNamePas(String projectNamePas) {
		this.projectNamePas = projectNamePas;
	}

	public Employee getBusinessOwnerPas() {
		return businessOwnerPas;
	}

	public void setBusinessOwnerPas(Employee businessOwnerPas) {
		this.businessOwnerPas = businessOwnerPas;
	}

	public Date getReleaseDatePas() {
		return releaseDatePas;
	}

	public void setReleaseDatePas(Date releaseDatePas) {
		this.releaseDatePas = releaseDatePas;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getActualReleaseDate() {
		return actualReleaseDate;
	}

	public void setActualReleaseDate(Date actualReleaseDate) {
		this.actualReleaseDate = actualReleaseDate;
	}

	public Employee getContactPersonPas() {
		return contactPerson;
	}

	public void setContactPerson(Employee contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getTechnologies() {
		return technologies;
	}

	public void setTechnologies(String technologies) {
		this.technologies = technologies;
	}

	public boolean isLookingForPeople() {
		return lookingForPeople;
	}

	public void setLookingForPeople(boolean lookingForPeople) {
		this.lookingForPeople = lookingForPeople;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "{id:" + projectId + "}";
	}
}