package com.endava.innovationlabs.gameofcodes.api;

import com.endava.innovationlabs.gameofcodes.model.Employee;
import com.endava.innovationlabs.gameofcodes.model.EmployeeProject;
import com.endava.innovationlabs.gameofcodes.model.TableMetadata;
import com.endava.innovationlabs.gameofcodes.service.EmployeeProjectService;
import com.endava.innovationlabs.gameofcodes.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/employee_project")
public class EmployeeProjectController {

    @Autowired
    private EmployeeProjectService employeeProjectService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<EmployeeProject>> getEmployeeProjects() {
        try {
            return new ResponseEntity<>(employeeProjectService.getAllEmployeeProjects(), HttpStatus.OK);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ArrayList<EmployeeProject>(), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "employee_id/{id}",method = RequestMethod.GET)
    public ResponseEntity<List<EmployeeProject>> getEmployeeProjectByEmployeeId(@PathVariable Integer id){
        return new ResponseEntity<>(employeeProjectService.getEmployeeProjectByEmployeeId(id), HttpStatus.OK);
    }

    @RequestMapping(value = "project_id/{id}",method = RequestMethod.GET)
    public ResponseEntity<List<EmployeeProject>> findEmployeeProjectsByProjectId(@PathVariable Integer id){
        return new ResponseEntity<>(employeeProjectService.getEmployeeProjectByEmployeeId(id), HttpStatus.OK);
    }

}
