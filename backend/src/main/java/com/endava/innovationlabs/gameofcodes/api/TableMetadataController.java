package com.endava.innovationlabs.gameofcodes.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.endava.innovationlabs.gameofcodes.model.TableMetadata;
import com.endava.innovationlabs.gameofcodes.service.TableMetadataService;

@RestController
@RequestMapping(value = "/api/columns")
public class TableMetadataController {

    @Autowired
    TableMetadataService tableMetadataService;
    
    @CrossOrigin
    @RequestMapping(value = "specific_columns",method = RequestMethod.GET)
    public ResponseEntity<List<TableMetadata>> findSpecificColumnNames(){
        return new ResponseEntity<>(tableMetadataService.getSpecificColumnNames(), HttpStatus.OK);
    }
    
    @CrossOrigin
    @RequestMapping(value = "specific_columns_employees",method = RequestMethod.GET)
    public ResponseEntity<List<TableMetadata>> findSpecificColumnNamesEmployees(){
        return new ResponseEntity<>(tableMetadataService.getSpecificColumnNamesEmployees(), HttpStatus.OK);
    }
    
    @CrossOrigin
    @RequestMapping(value = "specific_columns_projects",method = RequestMethod.GET)
    public ResponseEntity<List<TableMetadata>> findSpecificColumnNamesProjects(){
        return new ResponseEntity<>(tableMetadataService.getSpecificColumnNamesProjects(), HttpStatus.OK);
    }
}
