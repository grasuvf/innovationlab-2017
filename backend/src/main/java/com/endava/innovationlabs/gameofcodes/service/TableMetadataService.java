package com.endava.innovationlabs.gameofcodes.service;

import com.endava.innovationlabs.gameofcodes.dao.TableMetadataDAO;
import com.endava.innovationlabs.gameofcodes.model.TableMetadata;
import com.endava.innovationlabs.gameofcodes.repository.TableMetadataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TableMetadataService {

    @Autowired
    private TableMetadataRepository tableMetadataRepository;

    @Autowired
    private TableMetadataDAO tableMetadataDAO;

    public List<TableMetadata> getColumnNames(String tableName) {
        return tableMetadataRepository.findTableMetadatasByTableName(tableName);
    }

    public List<TableMetadata> getSpecificColumnNames() {
        return tableMetadataDAO.findSpecificTableMetadataColumns();
    }
    
	public List<TableMetadata> getSpecificColumnNamesEmployees() {
		return tableMetadataDAO.findSpecificTableMetadataEmployeesColumns();
	}

	public List<TableMetadata> getSpecificColumnNamesProjects() {
		return tableMetadataDAO.findSpecificTableMetadataProjectsColumns();
	}


    @Transactional
    public void updateColumnDisplayName(String tableName, String columnName, String columnDisplayName) {
        TableMetadata tableMetadata = tableMetadataDAO.findTableMetadataById(new TableMetadata.TableMetadataKey(columnName, tableName));
        tableMetadata.setColumnDisplayName(columnDisplayName);
        this.refreshColumnsDisplayName();
    }

    @Transactional
    public void refreshColumnsDisplayName() {
        List<TableMetadata> tableMetadataList = tableMetadataRepository.findAll();

        for (TableMetadata tableMetadata : tableMetadataList) {
            if (tableMetadata.getColumnDisplayName() == null) {
                String columnDisplayName = tableMetadata.getColumnName().replace('_', ' ');
                String[] arr = columnDisplayName.split(" ");
                StringBuffer sb = new StringBuffer();

                for (String s : arr) {
                    if (s.equals("pas")) {
                        sb.append('(').append(s.toUpperCase()).append(')');
                        tableMetadata.setEditable(false);
                    } else {
                        sb.append(Character.toUpperCase(s.charAt(0)));
                        sb.append(s.substring(1)).append(" ");
                        tableMetadata.setEditable(true);
                    }
                }

                tableMetadata.setColumnDisplayName(sb.toString().trim());
            }
        }
    }
}
