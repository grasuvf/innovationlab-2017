package com.endava.innovationlabs.gameofcodes;

import com.endava.innovationlabs.gameofcodes.service.TableMetadataService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class GameofcodesApplication {
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(GameofcodesApplication.class, args);

		context.getBean(TableMetadataService.class).refreshColumnsDisplayName();
	}
}
