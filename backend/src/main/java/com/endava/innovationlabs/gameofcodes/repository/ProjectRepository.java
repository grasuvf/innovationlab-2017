package com.endava.innovationlabs.gameofcodes.repository;

import com.endava.innovationlabs.gameofcodes.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
    public Project findProjectByProjectId(Integer projectId);
    public List<Project> findProjectByActualReleaseDate(Date actualReleaseDate);
    public List<Project> findProjectByProjectNamePas(String projectNamePas);
}
