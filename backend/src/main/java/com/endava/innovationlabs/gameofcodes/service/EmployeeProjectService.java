package com.endava.innovationlabs.gameofcodes.service;

import com.endava.innovationlabs.gameofcodes.dao.EmployeeProjectDAO;
import com.endava.innovationlabs.gameofcodes.model.EmployeeProject;
import com.endava.innovationlabs.gameofcodes.repository.EmployeeProjectRepository;
import com.endava.innovationlabs.gameofcodes.repository.TableMetadataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeProjectService {

    @Autowired
    private EmployeeProjectRepository employeeProjectRepository;

    @Autowired
    private TableMetadataRepository tableMetadataRepository;

    @Autowired
    private EmployeeProjectDAO employeeProjectDAO;

    public List<EmployeeProject> getAllEmployeeProjects() {
        return employeeProjectRepository.findAll();
    }
    public List<EmployeeProject> getEmployeeProjectByEmployeeId(Integer id){
        return employeeProjectDAO.findEmployeeProjectsByEmployeeId(id);
    }

//    public EmpvloyeeProject getEmployeeProjectById(Integer id) {
//        return employeeProjectRepository.findEmployeeProjectsByEmployeeId(id);
//    }

//    public List getColumnNames() {
//        return tableMetadataRepository.findTableMetadatasByTableName("employees");
//    }

}
