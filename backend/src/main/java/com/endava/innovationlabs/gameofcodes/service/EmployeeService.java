package com.endava.innovationlabs.gameofcodes.service;

import com.endava.innovationlabs.gameofcodes.dao.EmployeeDAO;
import com.endava.innovationlabs.gameofcodes.model.Employee;
import com.endava.innovationlabs.gameofcodes.repository.EmployeeRepository;
import com.endava.innovationlabs.gameofcodes.repository.TableMetadataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeService.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TableMetadataRepository tableMetadataRepository;

    @Autowired
    private EmployeeDAO employeeDAO;

    public List<Employee> getAllEmployees() {
        LOGGER.info("Getting all employees");
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(Integer id) {
        LOGGER.info("Getting employee by id: " + id);
        return employeeRepository.findEmployeesByEmployeeId(id);
    }

    public void updateEmployee(Employee employee) {
        LOGGER.info("Updating employee with id: " + employee.getEmployeeId());
        employeeRepository.save(employee);
    }
}
