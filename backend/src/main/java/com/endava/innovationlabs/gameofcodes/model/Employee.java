package com.endava.innovationlabs.gameofcodes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "employees")
public class Employee implements Serializable {

    @OneToMany(mappedBy = "employee")
    private Set<EmployeeProject> employeeProjects = new HashSet<EmployeeProject>();

    @Id
    private Integer employeeId;

    @Column(name = "id_pas")
    private String idPas;

    @Column(name = "employee_name")
    private String employeeName;

    @Column(name = "employee_name_pas")
    private String employeeNamePas;

    @Column(name = "grade_pas")
    private String gradePas;

    @Column(name = "project_code_pas")
    private String projectCodePas;

    @Column(name = "discipline")
    private String discipline;

    @Column(name = "core_skill")
    private String coreSkill;

    @Column(name = "booking_status")
    private String bookingStatus;

    @Column(name = "allocation_status")
    private String allocationStatus;

    @Temporal(TemporalType.DATE)
    @Column(name = "actual_release_date")
    private Date actualReleaseDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "release_date")
    private Date releaseDate;

    @Column(name = "booked")
    private String booked;

    @Temporal(TemporalType.DATE)
    @Column(name = "booked_at_date")
    private Date bookedAtDate;

    @Column(name = "soft_booked_for")
    private String softBookedFor;

    @Column(name = "booked_for")
    private String bookedFor;

    @Column(name = "color")
    private String color;

    @Column(name = "username_pas")
    private String username;

    @Column(name = "maternity_leaver")
    private String maternityLeaver;

    @Column(name = "discipline_main_skill")
    private String disciplineMainSkill;

    @Column(name = "employee_comments")
    private String employeeComments;

    @Column(name = "step_to_grade")
    private String stepToGrade;

    @Column(name = "endava_profile")
    private String endavaProfile;

    @Column(name = "skill_extended")
    private String skillExtended;

    @Column(name = "leadership_skills")
    private String leadershipSkills;

    @Column(name = "people_manager")
    private String peopleManager;

    @JsonIgnore
    public Set<EmployeeProject> getEmployeeProjects() {
        return employeeProjects;
    }

    public void setEmployeeProjects(Set<EmployeeProject> employeeProjects) {
        this.employeeProjects = employeeProjects;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getIdPas() {
        return idPas;
    }

    public void setIdPas(String idPas) {
        this.idPas = idPas;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeNamePas() {
        return employeeNamePas;
    }

    public void setEmployeeNamePas(String employeeNamePas) {
        this.employeeNamePas = employeeNamePas;
    }

    public String getGradePas() {
        return gradePas;
    }

    public void setGradePas(String gradePas) {
        this.gradePas = gradePas;
    }

    public String getProjectCodePas() {
        return projectCodePas;
    }

    public void setProjectCodePas(String projectCodePas) {
        this.projectCodePas = projectCodePas;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getCoreSkill() {
        return coreSkill;
    }

    public void setCoreSkill(String coreSkill) {
        this.coreSkill = coreSkill;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getAllocationStatus() {
        return allocationStatus;
    }

    public void setAllocationStatus(String allocationStatus) {
        this.allocationStatus = allocationStatus;
    }

    public Date getActualReleaseDate() {
        return actualReleaseDate;
    }

    public void setActualReleaseDate(Date actualReleaseDate) {
        this.actualReleaseDate = actualReleaseDate;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getBooked() {
        return booked;
    }

    public void setBooked(String booked) {
        this.booked = booked;
    }

    public Date getBookedAtDate() {
        return bookedAtDate;
    }

    public void setBookedAtDate(Date bookedAtDate) {
        this.bookedAtDate = bookedAtDate;
    }

    public String getSoftBookedFor() {
        return softBookedFor;
    }

    public void setSoftBookedFor(String softBookedFor) {
        this.softBookedFor = softBookedFor;
    }

    public String getBookedFor() {
        return bookedFor;
    }

    public void setBookedFor(String bookedFor) {
        this.bookedFor = bookedFor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMaternityLeaver() {
        return maternityLeaver;
    }

    public void setMaternityLeaver(String maternityLeaver) {
        this.maternityLeaver = maternityLeaver;
    }

    public String getDisciplineMainSkill() {
        return disciplineMainSkill;
    }

    public void setDisciplineMainSkill(String disciplineMainSkill) {
        this.disciplineMainSkill = disciplineMainSkill;
    }

    public String getEmployeeComments() {
        return employeeComments;
    }

    public void setEmployeeComments(String employeeComments) {
        this.employeeComments = employeeComments;
    }

    public String getStepToGrade() {
        return stepToGrade;
    }

    public void setStepToGrade(String stepToGrade) {
        this.stepToGrade = stepToGrade;
    }

    public String getEndavaProfile() {
        return endavaProfile;
    }

    public void setEndavaProfile(String endavaProfile) {
        this.endavaProfile = endavaProfile;
    }

    public String getSkillExtended() {
        return skillExtended;
    }

    public void setSkillExtended(String skillExtended) {
        this.skillExtended = skillExtended;
    }

    public String getLeadershipSkills() {
        return leadershipSkills;
    }

    public void setLeadershipSkills(String leadershipSkills) {
        this.leadershipSkills = leadershipSkills;
    }

    public String getPeopleManager() {
        return peopleManager;
    }

    public void setPeopleManager(String peopleManager) {
        this.peopleManager = peopleManager;
    }

}