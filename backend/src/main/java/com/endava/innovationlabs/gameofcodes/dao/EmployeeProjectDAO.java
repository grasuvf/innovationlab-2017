package com.endava.innovationlabs.gameofcodes.dao;

import com.endava.innovationlabs.gameofcodes.model.EmployeeProject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class EmployeeProjectDAO extends AbstractDAO {

    protected EmployeeProjectDAO() {
        super(EmployeeProject.class);
    }

    public List<EmployeeProject> findEmployeeProjectsByEmployeeId(Integer employeeId) {
        return entityManager.createQuery("SELECT ep FROM EmployeeProject ep WHERE ep.employee.employeeId = :id")
                .setParameter("id", employeeId)
                .getResultList();
    }

    public List<EmployeeProject> findEmployeeProjectsByProjectId(Integer projectId){
        return entityManager.createQuery("SELECT ep FROM EmployeeProject ep WHERE ep.project.projectId = :id")
                .setParameter("id", projectId)
                .getResultList();
    }
}