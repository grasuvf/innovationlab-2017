package com.endava.innovationlabs.gameofcodes.api;

import com.endava.innovationlabs.gameofcodes.model.Project;
import com.endava.innovationlabs.gameofcodes.model.TableMetadata;
import com.endava.innovationlabs.gameofcodes.service.ProjectService;
import com.endava.innovationlabs.gameofcodes.service.TableMetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/projects")
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @Autowired
    private TableMetadataService tableMetadataService;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getProjects() {
    	return new ResponseEntity<>(projectService.getAllProjects(), HttpStatus.OK);
    }

    @RequestMapping(value = "columnNames", method = RequestMethod.GET)
    public ResponseEntity<List<TableMetadata>> getColumnNames() {
        return new ResponseEntity<>(projectService.getColumnNames(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @CrossOrigin
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<Project> getById(@PathVariable Integer id) {
        return new ResponseEntity<>(projectService.getProjectById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "column_name/{columnName}/{newColumnName}", method = RequestMethod.PUT)
    public ResponseEntity updateColumnDisplayName(@PathVariable String columnName, @PathVariable String newColumnName) {
        try {
            tableMetadataService.updateColumnDisplayName("projects", columnName, newColumnName);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity("{exception:" + e.getStackTrace() +"}", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value= "{id}", method = RequestMethod.PUT)
    public ResponseEntity updateProject (@RequestBody Project project)
    {
        projectService.updateProject(project);
        return new ResponseEntity(HttpStatus.OK);
    }
}
