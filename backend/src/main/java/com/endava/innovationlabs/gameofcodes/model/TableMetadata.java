package com.endava.innovationlabs.gameofcodes.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(TableMetadata.TableMetadataKey.class)
@Table(name = "table_metadata")
public class TableMetadata implements Serializable {
    @Id
    @Column(name = "column_name")
    private String columnName;

    @Id
    @Column(name = "table_name")
    private String tableName;

    @Column(name = "column_display_name")
    private String columnDisplayName;

    @Column(name = "is_editable")
    private Boolean isEditable;

    public static class TableMetadataKey implements Serializable {
        TableMetadataKey() {
        }

        public TableMetadataKey(String columnName, String tableName) {
            this.columnName = columnName;
            this.tableName = tableName;
        }

        String columnName;
        String tableName;

        public String getColumnName() {
            return columnName;
        }

        public void setColumnName(String columnName) {
            this.columnName = columnName;
        }

        public String getTableName() {
            return tableName;
        }

        public void setTableName(String tableName) {
            this.tableName = tableName;
        }
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnDisplayName() {
        return columnDisplayName;
    }

    public void setColumnDisplayName(String columnDisplayName) {
        this.columnDisplayName = columnDisplayName;
    }

    public Boolean getEditable() {
        return isEditable;
    }

    public void setEditable(Boolean editable) {
        isEditable = editable;
    }

}
