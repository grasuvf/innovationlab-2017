package com.endava.innovationlabs.gameofcodes.repository;

import com.endava.innovationlabs.gameofcodes.model.EmployeeProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface EmployeeProjectRepository extends JpaRepository<EmployeeProject, Integer> {

	public EmployeeProject findEmployeeProjectsByJoinDate(String joinDate);
	public EmployeeProject findEmployeeProjectsByLeaveDate (Date leaveDate);
	public EmployeeProject findEmployeeProjectsByProjectRole(String projectRole);
}
