package com.endava.innovationlabs.gameofcodes.dao;

import com.endava.innovationlabs.gameofcodes.model.Employee;
import com.endava.innovationlabs.gameofcodes.model.TableMetadata;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TableMetadataDAO extends AbstractDAO {

	protected TableMetadataDAO() {
		super(Employee.class);
	}

	public TableMetadata findTableMetadataById(TableMetadata.TableMetadataKey tableMetadataKey) {
		return entityManager.find(TableMetadata.class, tableMetadataKey);
	}

	public List<TableMetadata> findSpecificTableMetadataColumns(){
		return entityManager.createQuery("SELECT columnName, columnDisplayName FROM TableMetadata")
				.getResultList();
	}

	public List<TableMetadata> findSpecificTableMetadataEmployeesColumns() {
		return entityManager.createQuery("SELECT columnName, columnDisplayName FROM TableMetadata WHERE tableName = 'employees'")
				.getResultList();
	}

	public List<TableMetadata> findSpecificTableMetadataProjectsColumns() {
		return entityManager.createQuery("SELECT columnName, columnDisplayName FROM TableMetadata WHERE tableName = 'projects'")
				.getResultList();
	}
}