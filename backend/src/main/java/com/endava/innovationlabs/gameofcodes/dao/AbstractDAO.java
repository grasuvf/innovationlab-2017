package com.endava.innovationlabs.gameofcodes.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

@Transactional
public abstract class AbstractDAO <E extends Serializable> {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeDAO.class);

    @PersistenceContext
    protected EntityManager entityManager;

    private final Class<E> entityClass;

    protected AbstractDAO(final Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    public E findOne( Integer id ){
        logger.info("Finding " + entityClass.getName() + " object with id = " + id);

        return (E) entityManager.find(entityClass, id);
    }

    public void save( E entity ){
        logger.info("Saving the " + entityClass.getName());

        if (entityManager.contains(entity)){
            entityManager.persist(entity);
        } else{
            entityManager.merge(entity);
        }
    }

    public void update( E entity ){
        entityManager.merge( entity );
    }

    public void delete( E entity ){
        entityManager.remove( entityManager.merge(entity) );
    }
}