package com.endava.innovationlabs.gameofcodes.dao;

import com.endava.innovationlabs.gameofcodes.model.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class EmployeeDAO extends AbstractDAO {

    protected EmployeeDAO() {
        super(Employee.class);
    }
}