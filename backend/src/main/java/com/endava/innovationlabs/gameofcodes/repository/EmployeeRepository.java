package com.endava.innovationlabs.gameofcodes.repository;

import com.endava.innovationlabs.gameofcodes.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	public Employee findEmployeesByEmployeeId(Integer employeeId);
	public List<Employee> findEmployeesByDiscipline(String discipline);
	public List<Employee> findEmployeesByCoreSkill(String coreSkill);
	public List<Employee> findEmployeesByEmployeeName(String employeeName);
	public List<Employee> findEmployeesByBookingStatus(String bookingStatus);
	public List<Employee> findEmployeesByAllocationStatus(String allocationStatus);
	public List<Employee> findEmployeesByActualReleaseDate(String actualReleaseDate);
	public List<Employee> findEmployeesByReleaseDate(String releaseDate);
	public List<Employee> findEmployeesByBooked(String booked);
	public List<Employee> findEmployeesByBookedAtDate(String bookedAtDate);
	public List<Employee> findEmployeesBySoftBookedFor(String softBookedFor);
	public List<Employee> findEmployeesByBookedFor(String bookedFor);
	public List<Employee> findEmployeesByColor(String color);
	public List<Employee> findEmployeesByUsername(String username);
	public List<Employee> findEmployeesByMaternityLeaver(String maternityLeaver);
	public List<Employee> findEmployeesByDisciplineMainSkill(String disciplineMainSkill);
	public List<Employee> findEmployeesByStepToGrade(String stepToGrade);
	public List<Employee> findEmployeesByEndavaProfile(String endavaProfile);
	public List<Employee> findEmployeesBySkillExtended(String skillExtended);
	public List<Employee> findEmployeesByLeadershipSkills(String leadershipSkills);
	public List<Employee> findEmployeesByPeopleManager(String peopleManager);
}
