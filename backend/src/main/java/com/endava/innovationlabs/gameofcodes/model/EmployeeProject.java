package com.endava.innovationlabs.gameofcodes.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "employee_project")
public class EmployeeProject implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;

	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;

	@Temporal(TemporalType.DATE)
	@Column(name = "join_date")
	private Date joinDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "leave_date")
	private Date leaveDate;

	@Column(name = "project_role")
	private String projectRole;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	// TODO add to serialization object
	public Integer getEmployeeId() {
		return employee.getEmployeeId();
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getProjectId() {
		return project.getProjectId();
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public Date getLeaveDate() {
		return leaveDate;
	}

	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	public String getProjectRole() {
		return projectRole;
	}

	public void setProjectRole(String projectRole) {
		this.projectRole = projectRole;
	}

	@Override
	public String toString() {
		return "{id:" + id + "}";
	}
}
