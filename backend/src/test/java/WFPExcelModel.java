public class WFPExcelModel {
    private int employee_id = 0;
    private String id_pas;
    private String employee_name;
    private String employee_name_pas;
    private String grade_pas;
    private String agu_pas;
    private String project_code_pas;
    private String account_pas;
    private String project_name_pas;
    private String business_owner_pas;
    private String discipline;
    private String core_skill;
    private String booking_status;
    private String allocation_status;
    private String actual_release_date;
    private String release_date;
    private String release_date_pas;
    private String booked;
    private String booked_at_date;
    private String soft_booked_for;
    private String booked_for;
    private String color;
    private String username_pas;
    private String maternity_leaver;
    private String discipline_main_skill;
    private String employee_comments;
    private String step_to_grade;
    private String endava_profile;
    private String skill_extended;
    private String leadership_skills;
    private int people_manager = 1;

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public String getId_pas() {
        if (id_pas == null || id_pas == "null") {
            return id_pas;
        } else {
            return "'"+id_pas+"'";
        }
    }

    public void setId_pas(String id_pas) {
        this.id_pas = id_pas;
    }

    public String getEmployee_name() {
        if (employee_name == null || employee_name == "null") {
            return employee_name;
        } else {
            return "'"+employee_name+"'";
        }
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_name_pas() {
        if (employee_name_pas == null || employee_name_pas == "null") {
            return employee_name_pas;
        } else {
            return "'"+employee_name_pas+"'";
        }
    }

    public void setEmployee_name_pas(String employee_name_pas) {
        this.employee_name_pas = employee_name_pas;
    }

    public String getGrade_pas() {
        if (grade_pas == null || grade_pas == "null") {
            return grade_pas;
        } else {
            return "'"+grade_pas+"'";
        }
    }

    public void setGrade_pas(String grade_pas) {
        this.grade_pas = grade_pas;
    }

    public String getAgu_pas() {
        if (agu_pas == null || agu_pas == "null") {
            return agu_pas;
        } else {
            return "'"+agu_pas+"'";
        }
    }

    public void setAgu_pas(String agu_pas) {
        this.agu_pas = agu_pas;
    }

    public String getProject_code_pas() {
        if (project_code_pas == null || project_code_pas == "null") {
            return project_code_pas;
        } else {
            return "'"+project_code_pas+"'";
        }
    }

    public void setProject_code_pas(String project_code_pas) {
        this.project_code_pas = project_code_pas;
    }

    public String getAccount_pas() {
        if (account_pas == null || account_pas == "null") {
            return account_pas;
        } else {
            return "'"+account_pas+"'";
        }
    }

    public void setAccount_pas(String account_pas) {
        this.account_pas = account_pas;
    }

    public String getProject_name_pas() {
        if (project_name_pas == null || project_name_pas == "null") {
            return project_name_pas;
        } else {
            return "'"+project_name_pas+"'";
        }
    }

    public void setProject_name_pas(String project_name_pas) {
        this.project_name_pas = project_name_pas;
    }

    public String getBusiness_owner_pas() {
        if (business_owner_pas == null || business_owner_pas == "null") {
            return business_owner_pas;
        } else {
            return "'"+business_owner_pas+"'";
        }
    }

    public void setBusiness_owner_pas(String business_owner_pas) {
        this.business_owner_pas = business_owner_pas;
    }

    public String getDiscipline() {
        if (discipline == null || discipline == "null") {
            return discipline;
        } else {
            return "'"+discipline+"'";
        }
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getCore_skill() {
        if (core_skill == null || core_skill == "null") {
            return core_skill;
        } else {
            return "'"+core_skill+"'";
        }
    }

    public void setCore_skill(String core_skill) {
        this.core_skill = core_skill;
    }

    public String getBooking_status() {
        if (booking_status == null || booking_status == "null") {
            return booking_status;
        } else {
            return "'"+booking_status+"'";
        }
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    public String getAllocation_status() {
        if (allocation_status == null || allocation_status == "null") {
            return allocation_status;
        } else {
            return "'"+allocation_status+"'";
        }
    }

    public void setAllocation_status(String allocation_status) {
        this.allocation_status = allocation_status;
    }

    public String getActual_release_date() {
        if (actual_release_date == null || actual_release_date == "null") {
            return actual_release_date;
        } else {
            return "'"+actual_release_date+"'";
        }
    }

    public void setActual_release_date(String actual_release_date) {
        this.actual_release_date = actual_release_date;
    }

    public String getRelease_date() {
        if (release_date == null || release_date == "null") {
            return release_date;
        } else {
            return "'"+release_date+"'";
        }
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getRelease_date_pas() {
        if (release_date_pas == null || release_date_pas == "null") {
            return release_date_pas;
        } else {
            return "'"+release_date_pas+"'";
        }
    }

    public void setRelease_date_pas(String release_date_pas) {
        this.release_date_pas = release_date_pas;
    }

    public String getBooked() {
        if (booked == null || booked == "null") {
            return booked;
        } else {
            return "'"+booked+"'";
        }
    }

    public void setBooked(String booked) {
        this.booked = booked;
    }

    public String getBooked_at_date() {
        if (booked_at_date == null || booked_at_date == "null") {
            return booked_at_date;
        } else {
            return "'"+booked_at_date+"'";
        }
    }

    public void setBooked_at_date(String booked_at_date) {
        this.booked_at_date = booked_at_date;
    }

    public String getSoft_booked_for() {
        if (soft_booked_for == null || soft_booked_for == "null") {
            return soft_booked_for;
        } else {
            return "'"+soft_booked_for+"'";
        }
    }

    public void setSoft_booked_for(String soft_booked_for) {
        this.soft_booked_for = soft_booked_for;
    }

    public String getBooked_for() {
        if (booked_for == null || booked_for == "null") {
            return booked_for;
        } else {
            return "'"+booked_for+"'";
        }
    }

    public void setBooked_for(String booked_for) {
        this.booked_for = booked_for;
    }

    public String getColor() {
        if (color == null || color == "null") {
            return color;
        } else {
            return "'"+color+"'";
        }
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getUsername_pas() {
        if (username_pas == null || username_pas == "null") {
            return username_pas;
        } else {
            return "'"+username_pas+"'";
        }
    }

    public void setUsername_pas(String username_pas) {
        this.username_pas = username_pas;
    }

    public String getMaternity_leaver() {
        if (maternity_leaver == null || maternity_leaver == "null") {
            return maternity_leaver;
        } else {
            return "'"+maternity_leaver+"'";
        }
    }

    public void setMaternity_leaver(String maternity_leaver) {
        this.maternity_leaver = maternity_leaver;
    }

    public String getDiscipline_main_skill() {
        if (discipline_main_skill == null || discipline_main_skill == "null") {
            return discipline_main_skill;
        } else {
            return "'"+discipline_main_skill+"'";
        }
    }

    public void setDiscipline_main_skill(String discipline_main_skill) {
        this.discipline_main_skill = discipline_main_skill;
    }

    public String getEmployee_comments() {
        if (employee_comments == null || employee_comments == "null") {
            return employee_comments;
        } else {
            return "'"+employee_comments+"'";
        }
    }

    public void setEmployee_comments(String employee_comments) {
        this.employee_comments = employee_comments;
    }

    public String getStep_to_grade() {
        if (step_to_grade == null || step_to_grade == "null") {
            return step_to_grade;
        } else {
            return "'"+step_to_grade+"'";
        }
    }

    public void setStep_to_grade(String step_to_grade) {
        this.step_to_grade = step_to_grade;
    }

    public String getEndava_profile() {
        if (endava_profile == null || endava_profile == "null") {
            return endava_profile;
        } else {
            return "'"+endava_profile+"'";
        }
    }

    public void setEndava_profile(String endava_profile) {
        this.endava_profile = endava_profile;
    }

    public String getSkill_extended() {
        if (skill_extended == null || skill_extended == "null") {
            return skill_extended;
        } else {
            return "'"+skill_extended+"'";
        }
    }

    public void setSkill_extended(String skill_extended) {
        this.skill_extended = skill_extended;
    }

    public String getLeadership_skills() {
        if (leadership_skills == null || leadership_skills == "null") {
            return leadership_skills;
        } else {
            return "'"+leadership_skills+"'";
        }
    }

    public void setLeadership_skills(String leadership_skills) {
        this.leadership_skills = leadership_skills;
    }

    public int getPeople_manager() {
            return people_manager;
    }

    public void setPeople_manager(int people_manager) {
        this.people_manager = people_manager;
    }

    public void insertIntoItem(int colIndex, String s) {
        switch (colIndex){
            case 0:
                setDiscipline(s);
                break;
            case 1:
                setCore_skill(s);
                break;
            case 2:
                setId_pas(s);
                break;
            case 3:
                setEmployee_name_pas(s);
                break;
            case 4:
                setEmployee_name(s);
                break;
            case 5:
                setGrade_pas(s);
                break;
            case 6:
                setBooking_status(s);
                break;
            case 7:
                setAllocation_status(s);
                break;
            case 8:
                setAgu_pas(s);
                break;
            case 9:
                setProject_code_pas(s);
                break;
            case 10:
                setAccount_pas(s);
                break;
            case 11:
                setProject_name_pas(s);
                break;
            case 12:
                setBusiness_owner_pas(s);
                break;
            case 13:
                setActual_release_date(s);
                break;
            case 14:
                setRelease_date(s);
                break;
            case 15:
                setRelease_date_pas(s);
                break;
            case 16:
                setBooked(s);
                break;
            case 17:
                setBooked_at_date(s);
                break;
            case 18:
                setSoft_booked_for(s);
                break;
            case 19:
                setBooked_for(s);
                break;
            case 20:
                setColor(s);
                break;
            case 21:
                setUsername_pas(s);
                break;
            case 22:
                setMaternity_leaver(s);
                break;
            case 23:
                setDiscipline_main_skill(s);
                break;
            case 24:
                setEmployee_comments(s);
                break;
            case 25:
                setStep_to_grade(s);
                break;
            case 26:
                setEndava_profile(s);
                break;
            case 27:
                setSkill_extended(s);
                break;
            case 28:
                setLeadership_skills(s);
                break;
        }
    }
}
