import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

public class WFPToProjects {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream(".\\backend\\src\\test\\resources\\ISD WFP Availability.xlsx");

        // Finds the workbook instance for XLSX file
        Workbook myWorkBook = new XSSFWorkbook(fis);

        // Return first sheet from the XLSX workbook
        Sheet mySheet = myWorkBook.getSheetAt(0);

        ArrayList<WFPExcelModel> toInsert = new ArrayList<>();

        //mySheet.getLastRowNum() -> HARDCODAT
        for (int rowIndex = 1; rowIndex <= 391; rowIndex++) {
            Row row = mySheet.getRow(rowIndex);

            WFPExcelModel toInsertItem = new WFPExcelModel();

            if (row.getCell(9).getCellTypeEnum() != CellType.BLANK) {

                for (int colIndex = 0; colIndex < row.getLastCellNum(); colIndex++) {

                    Cell cell = row.getCell(colIndex);
                    FormulaEvaluator formulaEval = myWorkBook.getCreationHelper().createFormulaEvaluator();

                    if (cell.getCellTypeEnum() != CellType.BLANK) {
                        switch (cell.getCellTypeEnum()) {
                            case STRING:
                                toInsertItem.insertIntoItem(colIndex, cell.getStringCellValue());
                                break;
                            case NUMERIC:
                                toInsertItem.insertIntoItem(colIndex, String.valueOf(cell.getDateCellValue()));
                                break;
                            case FORMULA:
                                if (colIndex == 13) {
                                    toInsertItem.insertIntoItem(colIndex, formulaEval.evaluate(cell).getStringValue());
                                }
                        }
                    } else {
                        toInsertItem.insertIntoItem(colIndex, "null");
                    }
                }
            }
            toInsert.add(toInsertItem);
        }

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/goc",
                            "postgres", "postgres");
            System.out.println("Opened database successfully");

            for (int i = 0; i < toInsert.size(); i++) {
                stmt = c.createStatement();
                System.out.println(textToSQL(toInsert.get(i)));
                System.out.println("--Row: " + i);
                try {
                    stmt.executeUpdate(textToSQL(toInsert.get(i)));
                } catch (Exception e) {
/*                    e.getMessage();
                    e.printStackTrace()*/;
                    continue;
                }
            }
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Table created successfully");
    }

    public static String textToSQL(WFPExcelModel obj) {
        String sql = "INSERT INTO projects VALUES ("
                + "DEFAULT"
                + ", " + obj.getProject_code_pas()
                + ", " + obj.getAgu_pas()
                + ", " + obj.getAccount_pas()
                + ", " + obj.getProject_name_pas()
                + ", " + "1" //obj.getBusiness_owner_pas()
                + ", " + obj.getRelease_date_pas()
                + ", " + getRandomDescription() //description-ul
                + ", " + "'1/1/1999'" //start_date
                + ", " + obj.getActual_release_date()
                + ", " + "1" //contact_person
                + ", " + getRandomTechnologies() //technologies
                + ", " + "false" //looking_for_people
                + ", " + "'Iasi'" //city
                + ");";
        return sql;
    }
    
    private static String getRandomTechnologies() {
    	ArrayList<String> technologies = new ArrayList<String>();
    	technologies.add("Java");
    	technologies.add("Javascript");
    	technologies.add("Angular 1");
    	technologies.add("Angular 2");
    	technologies.add("Python");
    	technologies.add("Ruby");
    	technologies.add("Php");
    	technologies.add("HTML5");
    	technologies.add("CSS3");
    	technologies.add("C++");
    	technologies.add("C#");
    	technologies.add("ASP.NET");
    	technologies.add("PostgreSQL");
    	technologies.add("MongoDB");
    	technologies.add("Matlab");
    	technologies.add("Android");
    	technologies.add("Ruby on Rails");
    	technologies.add("SCALA");
    	technologies.add("Django");
    	technologies.add("Perl");
    	technologies.add("Visual Basic");
    	technologies.add("C");
    	technologies.add("Objective C");
    	technologies.add("Erlang");
    	technologies.add("Node.js");
    	technologies.add("Koa");
    	
    	int random1;
    	int random2;
    	while(true){
    		random1 = new Random().nextInt(technologies.size());
    		random2 = new Random().nextInt(technologies.size());
    		if(random1 != random2)
    			break;
    	}
    	
    	String result = "'" + technologies.get(random1) + "," + technologies.get(random2) + "'";
    	System.out.println(result);
    	return result;
    }

    private static String getRandomDescription() {
    	ArrayList<String> description = new ArrayList<String>();
    	description.add("'Sentiment Analysis in Twitter', the goal of the project is to develop an automated machine learning system for sentiment analysis in social media texts such as Twitter.");
    	description.add("We want to understand the development of the human visual system. This knowledge will help in the prevention and treatment of certain vision problems in children.");
    	description.add("We do not rely solely on animal experiments. We can obtain answers to some of the questions we pose from computer models designed to simulate various aspects of visual development.");
    	description.add("A major focus of our work is the pathway that leads from the two eyes to the visual centers of the brain, including the cerebral cortex.");
    	description.add("Another area of study is the pathway that links visual areas located in the two sides of the brain. In each case, our initial work is performed using normal adult animals, usually rats.");
    	description.add("Data we obtain from these adult animals are then compared with data from young, developing animals or from older animals that had abnormal visual experience early in life.");
    	description.add("Our goal is first to understand the normal arrangement of neural connections and then to assess how genetic and environmental factors guide the formation of these connections during development.");
    	description.add("The more we learn about development of the visual system, the more we realize the importance of events that occur at the very earliest stages in the formation of the central nervous system.");
    	description.add("Our experiments result in a better understanding of the biological rules that govern basic aspects of brain development in all mammals, including humans");
    	description.add("One of our most important discoveries is that a brief critical period exists shortly after birth when visual experience permanently modifies the properties of the visual system. ");
    	description.add("We could not have predicted this finding from computer models or from experiments on nerve cells cultured in a dish.");
    	description.add("This discovery helps us to understand some disorders of the human visual system such as amblyopia, also called lazy eye, a currently untreatable severe loss of vision in one eye.");
    	description.add("Amblyopia occurs when humans are exposed to abnormal visual experience early in life, during the critical period, because of disruption of any one of the delicate systems that control the focus or the movement of our eyes");
    	description.add("The county courthouse is a historic building that is located in the oldest town in the state. Over the past few years several localized floods have occurred in the two-block region surrounding the courthouse.");
    	description.add("This project will develop a module offered to level 2 Undergraduate students and will seek to develop student’s skills in collaborative working and information literacy while still advancing their discipline knowledge.");
    	description.add("The subject librarian will be invited to conduct an inquiry-based workshop with the students at the start of the module to prepare them for a literature search activity and will also provide ongoing support for students via discussion boards in the VLE.");
    	description.add("The second half of the module will focus on collaborative inquiry. Groups will negotiate a topic to investigate with the module leader and will be expected to produce a collaborative.");
    	description.add("You may want to read through some or all of the example proposals listed below. All of them were successful SURF proposals. (And they kindly gave me permission to use their proposals as examples.)");
    	description.add("The MIS student consulting team worked with IBM and their project sponsor located in the UK to extend the Rational DOORS methodology to support the specific needs of multi company development of medical devices.");
    	description.add("The key issues addressed included the unique document control and personnel scheduling needs of this scenario.  The specific issues related to appropriate management of intellectual property were given particular attention.");
    	description.add("The project was to create an interactive and content rich website that supports students in their efforts to visualize their future by using the neuroscience concept of Time Traveler.");
    	description.add("The student team worked on a proof of concept to develop a cloud based Business Intelligence Solutions for the Health Care industry tied to CRM.");
    	description.add("The team evaluated project portfolio management tool used by Microchip IT and developed go forward recommendations for greater use and value.");
    	description.add("Two student teams worked on applications and analytics projects that allowed Vantage West to better serve their customers.");
    	description.add("Students worked with Walmart to create a website that supported their use and administration of talent resources.");
    	description.add(" Several teams worked with a Fortune 50 global software developer, where one specific project looked at the productive aspect of IP reuse.");
    	
    	int random1 = new Random().nextInt(description.size());
    	
    	String result = "'" + description.get(random1) + "'";
    	System.out.println(result);
    	return result;
    }
    
}
