import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WFPToEmployees {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream(".\\backend\\src\\test\\resources\\ISD WFP Availability.xlsx");

        // Finds the workbook instance for XLSX file
        Workbook myWorkBook = new XSSFWorkbook(fis);

        // Return first sheet from the XLSX workbook
        Sheet mySheet = myWorkBook.getSheetAt(0);

        ArrayList<WFPExcelModel> toInsert = new ArrayList<>();


        //mySheet.getLastRowNum() -> HARDCODAT
        for (int rowIndex = 1; rowIndex <= 398; rowIndex++) {
            Row row = mySheet.getRow(rowIndex);

            WFPExcelModel toInsertItem = new WFPExcelModel();

            if (row.getCell(3).getCellTypeEnum() != CellType.BLANK) {

                for (int colIndex = 0; colIndex < row.getLastCellNum(); colIndex++) {

                    Cell cell = row.getCell(colIndex);
                    FormulaEvaluator formulaEval = myWorkBook.getCreationHelper().createFormulaEvaluator();

                    if (cell.getCellTypeEnum() != CellType.BLANK) {
                        switch (cell.getCellTypeEnum()) {
                            case STRING:
                                toInsertItem.insertIntoItem(colIndex, cell.getStringCellValue());
                                break;
                            case NUMERIC:
                                toInsertItem.insertIntoItem(colIndex, String.valueOf(cell.getDateCellValue()));
                                break;
                            case FORMULA:
                                switch (colIndex) {
                                    case 13:
                                        toInsertItem.insertIntoItem(colIndex, String.valueOf(cell.getDateCellValue()));
                                        break;
                                    case 20:
                                        toInsertItem.insertIntoItem(colIndex, String.valueOf(formulaEval.evaluate(cell).getNumberValue()));
                                        break;
                                    default:
                                        toInsertItem.insertIntoItem(colIndex, formulaEval.evaluate(cell).getStringValue());
                                        break;
                                }
                        }
                    } else {
                        toInsertItem.insertIntoItem(colIndex, "null");
                    }
                }
            }
            toInsert.add(toInsertItem);
        }

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/goc",
                            "postgres", "postgres");
            System.out.println("Opened database successfully");

            for (int i = 0; i < toInsert.size(); i++) {
                stmt = c.createStatement();
                System.out.println(textToSQL(toInsert.get(i)));
                try {
                    stmt.executeUpdate(textToSQL(toInsert.get(i)));
                } catch (Exception e){
//                    e.getMessage();
//                    e.printStackTrace();
                    continue;
                }
            }
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            System.exit(0);
        }
        System.out.println("Table created successfully");
    }

    public static String textToSQL(WFPExcelModel obj) {
        String sql = "INSERT INTO employees VALUES ("
                +"DEFAULT"
                +", " + obj.getId_pas()
                +", " + obj.getEmployee_name()
                +", " + obj.getEmployee_name_pas()
                +", " + obj.getGrade_pas()
                +", " + obj.getProject_code_pas()
                +", " + obj.getDiscipline()
                +", " + obj.getCore_skill()
                +", " + obj.getBooking_status()
                +", " + obj.getAllocation_status()
                +", " + obj.getActual_release_date()
                +", " + obj.getRelease_date()
                +", " + obj.getBooked()
                +", " + obj.getBooked_at_date()
                +", " + obj.getSoft_booked_for()
                +", " + obj.getBooked_for()
                +", " + obj.getColor()
                +", " + obj.getUsername_pas()
                +", " + obj.getMaternity_leaver()
                +", " + obj.getDiscipline_main_skill()
                +", " + obj.getEmployee_comments()
                +", " + obj.getStep_to_grade()
                +", " + obj.getEndava_profile()
                +", " + obj.getSkill_extended()
                +", " + obj.getLeadership_skills()
                +", " + obj.getPeople_manager()
                +");";

        return sql;
    }


}

