# Game of Codes

## Instalation
-required
 npm install -g @angular/cli@latest (Angular CLI)  

-dependencies (note: use ./mvnw in Linux terminal)  
 cd project_root  
 mvnw clean install [-DskipTests]  
 
-set up db  
 create new postgres db named "goc"  
 run the sql script from backend\src\main\resources\sql_scripts
  
-test data  
 go to src/test and run WFPToEmployees and WFPToProjects

-run backend  
 cd backend  
 mvnw spring-boot:run  
  
-run the frontend app separately  
 cd frontend/src/main/frontend  
 ng serve --open  
  
-run on server  
 java -jar target/backend-0.0.1-SNAPSHOT.jar